var ready = function() {
	$('.spaces-rented').hide();
	$('.information-view').hide();

	$('.renting-btn').click(function() {
		$('.renting-btn').removeClass('btn-default');
		$('.renting-btn').addClass('btn-primary');
		$('.renting-btn').addClass('active');
		$('.listings-btn').addClass('btn-default');
		$('.listings-btn').removeClass('btn-primary');
		$('.listings-btn').removeClass('active');
		$('.spaces-rented').show();
		$('.spaces-listed').hide();
	});

	$('.listings-btn').click(function() {
		$('.listings-btn').removeClass('btn-default');
		$('.listings-btn').addClass('btn-primary');
		$('.listings-btn').addClass('active');
		$('.renting-btn').addClass('btn-default');
		$('.renting-btn').removeClass('btn-primary');
		$('.renting-btn').removeClass('active');
		$('.spaces-listed').show();
		$('.spaces-rented').hide();
	});

	$('.spaces-tab').click(function() {
		$('.information-view').hide();
		$('.spaces-view').show();
		$('.information-tab').removeClass('active');
		$('.spaces-tab').addClass('active');
	});

	$('.information-tab').click(function() {
		$('.spaces-tab').removeClass('active');
		$('.information-tab').addClass('active');
		$('.spaces-view').hide();
		$('.information-view').show();
	});
};

$(document).ready(ready);
$(document).on('page:load', ready);

