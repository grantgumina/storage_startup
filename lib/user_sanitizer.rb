class User::ParameterSanitizer < Devise::ParameterSanitizer
  private
  def account_update
    params.require(:user).permit(:first_name, :last_name, :phone, :email, :password, :password_confirmation, :current_password)
  end
end
