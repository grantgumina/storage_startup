class Listing < ActiveRecord::Base
  belongs_to :user

  scope :rented_by, ->(user_id) { where("renter_id IS ?", user_id) }

  def rented?
    renter_id.present?
  end

  def to_s
    "#{width * length} sq ft @ #{street_address}, #{city}, #{state} #{zip_code}"
  end

  def self.search(search)
    if search
      find(:all, :conditions => ['city LIKE ?', "%#{search}"])
    else
      find(:all)
    end
  end
end
