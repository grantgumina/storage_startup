class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :listings, dependent: :destroy

  def ready_to_buy?
    #TODO this should check that everything in their profile is accurate
    true
  end

  def buy listing
    # TODO charge their credit card, send out emails n shit
    listing.renter_id = id
    # Also TODO, never save!, always if save blah blah
    listing.save!
  end
end
