class UserController < Devise::SessionsController
  before_filter :authenticate_user!, :except => [:index]
  def index
    if signed_in?
      redirect_to action: "show", id: current_user.id
    else 
      redirect_to new_user_session_path
    end
  end

  def show
    id = params[:id]
    @title = "User Listings"
    @user_inspecting = User.find id
    @listings = @user_inspecting.listings

    if signed_in? and @user_inspecting.id == current_user.id
      @renting = Listing.rented_by @user_inspecting.id
    end
  end

  def edit

  end
end
