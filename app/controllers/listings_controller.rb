class ListingsController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]
  def index
    @title = "Listings"
    if !params[:search].nil? and params[:search] != ""
      @listings = Listing.where("listings.renter_id IS NULL")
      @listings = @listings.where(city: params[:search])
    elsif !params[:type].nil? and params[:type] != "" and params[:type] != "None"
      @listings = Listing.where("listings.renter_id IS NULL")
      @listings = @listings.where(storage_type: params[:type])
      if !params[:low_price].nil? and params[:low_price] != "" and !params[:high_price].nil? and params[:high_price] != ""
        @listings = @listings.where(:price => params[:low_price]..params[:high_price])
      end
    elsif !params[:low_price].nil? and params[:low_price] != "" and !params[:high_price].nil? and params[:high_price] != ""
      @listings = Listing.where("listings.renter_id IS NULL")
      @listings = @listings.where(:price => params[:low_price]..params[:high_price])
      if !params[:type].nil? and params[:type] != "" and params[:type] != "None"
        @listings = @listings.where(storage_type: params[:type])
      end
    else
      @listings = Listing.where("listings.renter_id IS NULL")
    end
  end

  def show
    @listing = Listing.find params[:id]
  end

  def new
    @title = "New Listing"
    @listing = Listing.new
  end

  def edit
    @title = "Edit Listing"
    @listing = Listing.find(params[:id])
  end

  def update
    
  end

  def create
    @listing = current_user.listings.create(listing_params)
    if @listing.save
      redirect_to action: :index
    else
      render :new
    end
  end

  # custom
  
  def rent
    @listing = Listing.find params[:id] 
    if @listing.rented?
      flash[:warning] = "Listing already rented"
      redirect_back_or_default root_path
    end

    if current_user.ready_to_buy?
      current_user.buy @listing
      flash[:notice] = "Successfully rented space!"
      redirect_to controller: "user", action: "show", id: current_user.id
    else
      flash[:warning] = "Please fill in the rest of your information"
      redirect_to controller: "user", action: "show", id: current_user.id
    end
  end

  private

  def listing_params
    params.require(:listing).permit(:start_date, :end_date, :street_address, :city, :state, :zip_code, :width, :length, :price, :storage_type)
  end
end
