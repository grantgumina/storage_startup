class AddLengthToListing < ActiveRecord::Migration
  def change
    remove_column :listings, :height, :integer
    add_column :listings, :length, :integer
  end
end
