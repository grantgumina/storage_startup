class CreateListings < ActiveRecord::Migration
  def change
    create_table :listings do |t|
      t.string :street_address
      t.string :state
      t.integer :zip_code
      t.integer :width
      t.integer :height
      t.integer :renter_id

      t.timestamps
    end
  end
end
