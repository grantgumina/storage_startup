class AddStorageTypeToListings < ActiveRecord::Migration
  def change
    add_column :listings, :storage_type, :string
  end
end
