class AddRangeToListing < ActiveRecord::Migration
  def change
    add_column :listings, :start_date, :datetime
    add_column :listings, :end_date, :datetime
  end
end
