Storage::Application.routes.draw do

  devise_for :users, :controllers => {:sessions => 'user'}

  devise_scope :user do
    get 'users/:id' => 'user#show'
    get 'user' => 'user#index'
  end

  resources :listings
  resources :user

  get 'listings/rent/:id' => "listings#rent"
  root 'app#index'

end
